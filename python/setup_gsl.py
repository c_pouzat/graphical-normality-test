from distutils.core import setup, Extension
from Cython.Build import cythonize

exts = cythonize([Extension("wrap_gsl",
                            sources=["wrap_gsl.pyx"],
                            libraries=["gsl","gslcblas"])])

setup(
    ext_modules = exts,
)
