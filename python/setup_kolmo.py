from distutils.core import setup, Extension
from Cython.Build import cythonize

exts = cythonize([Extension("wrap_kolmogorov_dist", 
                            sources=["k_no_main.c",
                                     "wrap_kolmogorov_dist.pyx"])])

setup(
    ext_modules = exts,
)
