# cython: language_level=3, embedsignature=True

cdef extern from "k_no_main.h":
    double K(int n,double d)
    
def pK(int n,double d):
    """Compute Kolmogorov's distribution K(n,d) = Prob(D_n < d)

    Parameters
    ----------
    n: sample size (python int)
    d: Kolmogorov's statistics value (python float)

    Returns
    -------
    K(n,d) = Prob(D_n < d)

    Details
    -------
    D_n = max(x_1-0/n,x_2-1/n...,x_n-(n-1)/n,1/n-x_1,2/n-x_2,...,n/n-x_n)
    with  x_1<x_2,...<x_n  a purported set of n independent uniform [0,1)
    random variables sorted into increasing order.
    See G. Marsaglia, Wai Wan Tsang and Jingbo Wong, J.Stat.Software (2003).
    """
    return K(n,d)
