# cython: language_level=3, embedsignature=True

cdef extern from "<gsl/gsl_cdf.h>":
    double gsl_cdf_ugaussian_Pinv(double P)
    double gsl_cdf_ugaussian_P(double x)
    double gsl_cdf_beta_P(double x, double a, double b)
    double gsl_cdf_beta_Pinv(double P, double a, double b)
    
def pbeta(double x, double a, double b):
    """Beta distribution CDF with shape parameters a and b

    Parameters
    ----------
    x: RV value at which the prob. is seeked, 0 <= x <= 1
       (python float)
    a: shape parameter > 0 (python float)
    b: shape parameter > 0 (python float)

    Returns
    -------
    the value of the CDF at x for a Beta distribution whose prob.
    density is defined by Γ(a+b)/Γ(a)/Γ(b) x^(a-1) (1-x)^(b-1)

    Details
    -------
    Uses the Gnu Scientific Library gsl_cdf_beta_P 
    function
    """
    if a <= 0.0 or b <= 0.0:
        raise ValueError
    if not 0.0 <= x <= 1.0:
        raise ValueError
    return gsl_cdf_beta_P(x, a, b)

def qbeta(double p, double a, double b):
    """Beta distribution quantile function with shape parameters a and b

    Parameters
    ----------
    p: probability value (python float)
    a: shape parameter > 0 (python float)
    b: shape parameter > 0 (python float)

    Returns
    -------
    a python float q such that Β(q,a,b) <= p, where Β(q,a,b) is
    the CDF at q for a Beta distribution whose prob.
    density is defined by Γ(a+b)/Γ(a)/Γ(b) x^(a-1) (1-x)^(b-1)

    Details
    -------
    Uses the Gnu Scientific Library gsl_cdf_beta_Pinv 
    function
    """
    if a <= 0.0 or b <= 0.0:
        raise ValueError
    if not 0.0 <= p <= 1.0:
        raise ValueError
    return gsl_cdf_beta_Pinv(p, a, b)

def pnorm(double x):
    """Cumulative Distribution Function (CDF) of a standard 
    normal distribution

    Parameters
    ----------
    x: RV value at which the prob. is seeked (python float)

    Returns
    -------
    a python float Phi(x), where Phi stands
    for the CDF of a standard normal distribution

    Details
    -------
    Uses the Gnu Scientific Library gsl_cdf_ugaussian_P 
    function
    """
    return gsl_cdf_ugaussian_P(x)

def qnorm(double p):
    """Quantile function of a standard normal distribution

    Parameters
    ----------
    p: probability value (python float)

    Returns
    -------
    a python float q such that Phi(q) <= p, where Phi stands
    for the CDF of a standard normal distribution

    Details
    -------
    Uses the Gnu Scientific Library gsl_cdf_ugaussian_Pinv 
    function
    """
    if not 0.0 <= p <= 1.0:
        raise ValueError
    return gsl_cdf_ugaussian_Pinv(p)
