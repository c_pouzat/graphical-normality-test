#ifndef __K_H__
#define __K_H__

double K(int n,double d) ;                        //Kolmogorov distribution
void mMultiply(double *A,double *B,double *C,int m) ;      //Matrix product
void mPower(double *A,int eA,double *V,int *eV,int m,int n); //Matrix power

#endif
