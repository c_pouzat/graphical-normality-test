cdef extern from "<gsl/gsl_errno.h>":
    cdef enum:
        GSL_SUCCESS  = 0
        GSL_FAILURE  = -1
        GSL_CONTINUE = -2 # iteration has not converged
        GSL_EDOM     = 1  # input domain error, e.g sqrt(-1) 
        GSL_ERANGE   = 2  # output range error, e.g. exp(1e100) 
        GSL_EFAULT   = 3  # invalid pointer 
        GSL_EINVAL   = 4  # invalid argument supplied by user
        GSL_EFAILED  = 5  # generic failure 
        GSL_EFACTOR  = 6  # factorization failed 
        GSL_ESANITY  = 7  # sanity check failed - shouldn't happen 
        GSL_ENOMEM   = 8  # malloc failed 
        GSL_EBADFUNC = 9  # problem with user-supplied function 
        GSL_ERUNAWAY = 10 # iterative process is out of control 
        GSL_EMAXITER = 11 # exceeded max number of iterations 
        GSL_EZERODIV = 12 # tried to divide by zero 
        GSL_EBADTOL  = 13 # user specified an invalid tolerance 
        GSL_ETOL     = 14 # failed to reach the specified tolerance
        GSL_EUNDRFLW = 15 # underflow
        GSL_EOVRFLW  = 16 # overflow 
        GSL_ELOSS    = 17 # loss of accuracy 
        GSL_EROUND   = 18 #  failed because of roundoff error
        GSL_EBADLEN  = 19 # matrix, vector lengths are not conformant 
        GSL_ENOTSQR  = 20 # matrix not square 
        GSL_ESING    = 21 # apparent singularity detected 
        GSL_EDIVERGE = 22 # integral or series is divergent
        GSL_EUNSUP   = 23 # requested feature is not supported by the hardware 
        GSL_EUNIMPL  = 24 # requested feature not (yet) implemented 
        GSL_ECACHE   = 25 # cache limit exceeded 
        GSL_ETABLE   = 26 # table limit exceeded
        GSL_ENOPROG  = 27 # iteration is not making progress towards solution
        GSL_ENOPROGJ = 28 # jacobian evaluations are not improving the solution
        GSL_ETOLF    = 29 # cannot reach the specified tolerance in F 
        GSL_ETOLX    = 30 # cannot reach the specified tolerance in X 
        GSL_ETOLG    = 31 # cannot reach the specified tolerance in gradient
        GSL_EOF      = 32 # end of file
        
    ctypedef void gsl_error_handler_t (const char * reason, const char * file,
                                       int line, int gsl_errno)
    gsl_error_handler_t * gsl_set_error_handler_off ()


cdef extern from "<gsl/gsl_vector.h>":
    ctypedef struct gsl_block:
        pass
    ctypedef struct gsl_vector:
        size_t size
        size_t stride
        double * data
        gsl_block * block
        int owner
    # Memory allocation
    gsl_vector * gsl_vector_alloc(size_t n)
    void gsl_vector_free(gsl_vector * v)
    # Accessing elts
    double gsl_vector_get(const gsl_vector * v, const size_t i) 
    void gsl_vector_set(gsl_vector * v, const size_t i, double x)
    # Intializing
    void gsl_vector_set_all(gsl_vector * v, double x)
    void gsl_vector_set_zero(gsl_vector * v)
    # Vector operations
    int gsl_vector_add(gsl_vector * a, const gsl_vector * b)
    int gsl_vector_sub(gsl_vector * a, const gsl_vector * b)
    int gsl_vector_mul(gsl_vector * a, const gsl_vector * b)
    int gsl_vector_div(gsl_vector * a, const gsl_vector * b)
    int gsl_vector_scale(gsl_vector * a, const double x)
    int gsl_vector_add_constant(gsl_vector * a, const double x)
    # Finding maximum and minimum elements of vectors
    double gsl_vector_max(const gsl_vector * v)
    double gsl_vector_min(const gsl_vector * v)
    size_t gsl_vector_max_index(const gsl_vector * v)
    size_t gsl_vector_min_index(const gsl_vector * v)
