# cython: language_level=3, embedsignature=True

cimport pygsl_vector

cdef class vector:
    cdef gsl_vector * _thisptr
    def __cinit__(self, size_t n, double initial_value):
        self._thisptr = gsl_vector_alloc (n)
        if self._thisptr is NULL:
            raise MemoryError()
        if initial_value == 0:    
            gsl_vector_set_zero(self._thisptr)
        else:
            gsl_vector_set_all(self._thisptr, initial_value)
    def __dealloc__(self):
        if self._thisptr is not NULL:
            gsl_vector_free(self._thisptr)
    cpdef double get(self, const size_t i) except? -1:
        gsl_set_error_handler_off()
        cdef double res = gsl_vector_get(self._thisptr, i)
        if res == 0. and i >= self._thisptr.size:
            raise IndexError("Index value too large")
        return res
    def __getitem__(self,i):
        return self.get(i)
    cpdef set(self, const size_t i, double x):
        if i >= self._thisptr.size:
            raise IndexError("Index value too large")
        gsl_vector_set(self._thisptr, i, x)
    def __setitem__(self,i,x):
        self.set(i, x)
    def scale(self, const double x):
        gsl_vector_scale(self._thisptr, x)
    def add_constant(self, const double x):
        gsl_vector_add_constant(self._thisptr, x)
    @property    
    def max(self):
        return gsl_vector_max(self._thisptr)
    @property    
    def min(self):
        return gsl_vector_min(self._thisptr)
    @property    
    def max_index(self):
        return gsl_vector_max_index(self._thisptr)
    @property    
    def min_index(self):
        return gsl_vector_min_index(self._thisptr)
    @property
    def len(self):
        return self._thisptr.size
    def __len__(self):
        return self._thisptr.size
    def add(self, vector other):
        gsl_vector_add(self._thisptr,other._thisptr)
    def __add__(self, other):
        if isinstance(other,vector): 
            self.add(other)
        elif isinstance(other,int) or isinstance(other,float):
            self.add_constant(other)
        else:
            print("Don't know how to perform the addition")
    def sub(self, vector other):
        gsl_vector_sub(self._thisptr,other._thisptr)
    def __sub__(self, other):
        if isinstance(other,vector): 
            self.sub(other)
        elif isinstance(other,int) or isinstance(other,float):
            self.add_constant(-other)
        else:
            print("Don't know how to perform the subtraction")
    def mul(self, vector other):
        gsl_vector_mul(self._thisptr,other._thisptr)
    def __mul__(self, other):
        if isinstance(other,vector): 
            self.mul(other)
        elif isinstance(other,int) or isinstance(other,float):
            self.scale(other)
        else:
            print("Don't know how to perform the multiplication")
    def div(self, vector other):
        gsl_vector_div(self._thisptr,other._thisptr)
    def __truediv__(self, other):
        if isinstance(other,vector): 
            self.div(other)
        elif isinstance(other,int) or isinstance(other,float):
            self.scale(1./other)
        else:
            print("Don't know how to perform the division") 
    def __str__(self):
        res = '<'
        if self._thisptr.size <= 10:
            for i in range(self._thisptr.size-1):
                res += str(self.get(i)) + ','
        else:
            for i in range(5):
                res += str(self.get(i)) + ','
            res += '...,'
            for i in range(-5,-1):
                res += str(self.get(self._thisptr.size+i-1)) + ','
        res += str(self.get(self._thisptr.size-1)) + '>'
        return res
