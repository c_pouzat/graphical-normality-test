from distutils.core import setup, Extension
from Cython.Build import cythonize

exts = cythonize([Extension("pygsl_vector",
                            sources=["pygsl_vector.pyx"],
                            libraries=["gsl","gslcblas"])])

setup(name="pygsl_vector",
      ext_modules = exts,
)
