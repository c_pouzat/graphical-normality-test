# graphical-normality-test

This is a  Python / Cython / C implementation of the graphical normality test proposed by Aldor-Noiman et al in [The Power to See: A New Graphical Test of Normality](https://repository.upenn.edu/statistics_papers/481/). The occasion for me to avoid `scipy` that I don't trust and use the [GNU Scientific Library](https://www.gnu.org/software/gsl/) (`GSL`) or over `C` code instead interfaced with `Python` through `Cython`.

For now, the key file is `tests_exploration.org` in the `python` directory.

# interfacing_vectors_and_matrices

This directory contains code showing how to interface [GSL](https://www.gnu.org/software/gsl/) [vectors and matrices](https://www.gnu.org/software/gsl/doc/html/vectors.html) in `Python`.
